include(gtest_dependency.pri)

TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG += thread
CONFIG -= qt

HEADERS += \
        factorial.h \


SOURCES += \
        factorial.cpp \
        main.cpp
